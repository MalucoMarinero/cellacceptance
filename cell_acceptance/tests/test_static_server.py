from flask import Flask, request, send_from_directory
import os
LOCAL = os.path.dirname(os.path.abspath(__file__))

server = Flask(__name__)
@server.route('/<path:filename>')
def send_file(filename):
    return send_from_directory('%s' % LOCAL, filename)

if __name__ == '__main__':
    server.run(debug=True, port=4200)
