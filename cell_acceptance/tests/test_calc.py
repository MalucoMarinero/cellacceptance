import cell_acceptance
import os
from unittest import TestCase
LOCAL = os.path.dirname(os.path.abspath(__file__))

class TestCalculator(TestCase):
    def test_basic(self):
        results = cell_acceptance.calc(
            '%s/test1.ods' % LOCAL,
            {'a2': [ [1], [2], [3], [4], [5], [6], [7], [8],
                     [9], [10], [11] ]},
            ["c12", "c11:c12"]
        )

        self.assertEqual(results[0], 99)
        self.assertEqual(results[1][0][0], 82.5)
        self.assertEqual(results[1][1][0], 99)

    def test_ranged(self):
        results = cell_acceptance.calc(
            '%s/test1.ods' % LOCAL,
            {'a2:a12': [ [1], [2], [3], [4], [5], [6], [7], [8],
                     [9], [10], [11] ]},
            ["c12", "c11:c12"]
        )

        self.assertEqual(results[0], 99)
        self.assertEqual(results[1][0][0], 82.5)
        self.assertEqual(results[1][1][0], 99)


